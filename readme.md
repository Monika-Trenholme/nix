# Nix OS Config
This is my Nix OS configuration.

## Build installer ISO
run ``` nixos-generate -f iso --flake .#iso -o result ```

## Installation
Run ``` init {disk} {device} ``` where {disk} is the disk configuration and {device} is the drive to install on.
This will format the drive, generate configuration files, and clone the repo.

Copy settings from /mnt/etc/nixos/install into the system configuration at /mnt/etc/nixos/hosts/{system}.
Most importantly overwrite hardware-configuration.nix and update the system.stateVersion value.
It may be beneficial to keep these files so the folder is listed in .gitignore

Run ``` strap {system} ``` to install the system.
