{ config, lib, pkgs, ... }:
with lib;
let
    oscfg = config.my-os;
    cfg = oscfg.services;
    netcfg = cfg.networking;
in
{
    options.my-os.services = with types; {
        audio.enable = mkEnableOption "audio";
        networking = {
            enable = mkEnableOption "networking";
            network-manager.enable = mkEnableOption "network manager";
            ssh.enable = mkEnableOption "ssh";
            allowedTCPPorts = mkOption {
            	type = listOf number;
            	description = "tcp ports to open in the firewall";
            	default = [];
            };
        };
        printing = {
            enable = mkEnableOption "printing";
            drivers = mkOption {
                type = listOf package;
                description = "printer drivers";
                default = [];
            };
        };
        uinput = {
            enable = mkEnableOption "kernel input emulation";
        };
        xdg-portal = {
            enable = mkEnableOption "XDG desktop portal";
        };
    };

    config = mkMerge [
        # Networking
        (mkIf cfg.networking.enable {
            networking = {
                networkmanager = {
                    enable = netcfg.network-manager.enable;
                    insertNameservers = [ "9.9.9.9" "1.1.1.1" ];
                };
                firewall = {
                    enable = true;
                    allowedTCPPorts = netcfg.allowedTCPPorts;
                };
	        };
	        services.openssh.enable = netcfg.ssh.enable;
			users.groups.networkmanager.members = oscfg.users.admins;
        })

        # Audio
        (mkIf cfg.audio.enable {
            security.rtkit.enable = true;
	        services.pipewire = {
		        enable = true;
		        alsa.enable = true;
		        jack.enable = true;
		        pulse.enable = true;
		        wireplumber.enable = true;
	        };
			users.groups.audio.members = oscfg.users.admins;
        })

        # Printing
        (mkIf cfg.printing.enable {
            services.printing = {
                enable = true;
                drivers = cfg.printing.drivers;
            };
            services.avahi = {
                enable = true;
                nssmdns4 = true;
                openFirewall = true;
            };
            hardware.sane.enable = true;
            users.groups = {
                lp.members = oscfg.users.admins;
                scanner.members = oscfg.users.admins;
            };
        })

        # Kernel Input Emulation
        (mkIf cfg.uinput.enable {
            hardware.uinput.enable = true;
            users.groups = {
                uinput.members = oscfg.users.admins;
                input.members = oscfg.users.admins;
            };
        })

        # XDG Desktop Portal
        (mkIf cfg.xdg-portal.enable {
            xdg.portal = {
                enable = true;
                wlr.enable = true;
                extraPortals = with pkgs; [
                    xdg-desktop-portal-hyprland
                ];
                config.common.default = "*";
            };
        })

        {
            # PAM
            security.pam.services.hyprlock = {};
        }
    ];
}
