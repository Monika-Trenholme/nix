{ config, lib, pkgs, ... }:
with lib;
let
    cfg = config.my-os;
in
{
    imports = [
        ./programs
        ./services.nix
    ];

    options.my-os = with types; {
        users = {
            autologin = mkOption {
                type = nullOr str;
                description = "";
                default = null;
            };
            admins = mkOption {
                type = listOf str;
                description = "admin accounts";
                default = [];
            };
        };
    };

    config = {
        # Locale
        time.timeZone = "America/Halifax";
        i18n.defaultLocale = "en_CA.UTF-8";

        hardware.graphics.enable = true;

        # System dark mode
        qt = {
            enable = true;
            platformTheme = "gnome";
            style = "adwaita-dark";
        };

        # Add admin users to "wheel" group for sudo privileges
        users.groups.wheel.members = cfg.users.admins;
        services.getty.autologinUser = mkForce cfg.users.autologin;

        # System level zsh integration
        # needed for default shell and completion support
        programs.zsh.enable = true;
        users.defaultUserShell = pkgs.zsh;
        environment.pathsToLink = [ "/share/zsh" ];
    };
}
