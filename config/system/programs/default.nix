{ lib, config, pkgs, inputs, ... }:
with lib;
let
    oscfg = config.my-os;
    cfg = oscfg.programs;
in
{
    imports = [ ./games.nix ];

    options.my-os.programs = with types; {
        college.enable = mkEnableOption "college";
        programming.enable = mkEnableOption "programming";
        virtualisation.enable = mkEnableOption "virtualisation";
        web.enable = mkEnableOption "web";
    };

    config = mkMerge [
        {
            environment.systemPackages = with pkgs; mkMerge [
                # Base program set
                [
                    # Basic utilities
                    htop smartmontools

                    # FileSystem support
                    exfatprogs
                ]

                # Programmer mode
                (mkIf cfg.programming.enable [
                    gcc rustup nasm
                ])

                # Web Apps
                (mkIf cfg.web.enable [
                    inputs.zen-browser.packages."${system}".default
                    stremio
                ])
            ];
        }

        # VMware
        (mkIf cfg.college.enable {
            virtualisation.vmware.host.enable = true;
            security.polkit = {
                extraConfig = ''
                    /* Allow any local user to do anything (dangerous!). */
                    polkit.addRule(function(action, subject) {
                        if (subject.local) return "yes";
                    });
                '';
            };
        })


        # Virtualisation
        (mkIf cfg.virtualisation.enable {
            programs.virt-manager.enable = true;
            users.groups.libvirtd.members = oscfg.users.admins;
            virtualisation = {
                libvirtd = {
                    enable = true;
                    onBoot = "start";
                };
                spiceUSBRedirection.enable = true;
            };

            # Autostart the virt-manager default network
            systemd.services.virsh-service = {
                wantedBy = [ "multi-user.target" ];
                after = [ "network.target" ];
                description = "virsh service";
                enable = true;
                script = ''
                    ${pkgs.libvirt}/bin/virsh net-autostart default
                '';
            };
        })
    ];
}
