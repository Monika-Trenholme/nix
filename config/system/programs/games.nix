{ config, lib, pkgs, ... }:
with lib;
let
    cfg = config.my-os.programs.games;
in
{
    options.my-os.programs.games = with types; {
        enable = mkEnableOption "games";
    };

    config = mkIf cfg.enable {
        environment.systemPackages = with pkgs; [
            itch
            osu-lazer-bin
            prismlauncher
            # vintagestory - need to wait for dotnet dependency bump

            # Bottles for Windows programs
            bottles
        ];
    };
}
