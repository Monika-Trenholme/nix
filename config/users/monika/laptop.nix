{ ... }:
{
    imports = [ ./desktop.nix ];
    my-home.de.bar.laptop = {
        enable = true;
        backlight = "acpi_video0";
    };
    my-home.dev.emacs.languages.all = true;
}
