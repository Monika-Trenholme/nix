{ outputs, ... }:
{
    imports = [ outputs.homeModules.default ];

    my-home.dev= {
        emacs.enable = true;
        git = {
            enable = true;
            user_name = "LPT4Dev";
            user_email = "";
        };
    };

    my-home.shell = {
        prompt = [
            {
                name = "username";
                style_user = "green";
                style_root = "red";
                format = "[$user]($style)";
                disabled = false;
                show_always = true;
            }{
                name = "hostname";
                format = " @ [$hostname](yellow)";
                disabled = false;
                ssh_only = false;
            }{
                name = "directory";
                format = " : [$path](blue)";
                home_symbol = "~";
                disabled = false;
            }
        ];
        fetch.enable = true;
    };
}
