{ ... }:
{
    imports = [ ./terminal.nix ];

    my-home.de = {
        enable = true;
        tags = [ "" "󰅩" "" "󰊖" "󰑈" "6" "7" "8" "9" "0" ];
        keyboard = "us";
        menu = {
            passwords.enable = true;
        };
        wm = {
            gaps_out = 0;
            auto_start = [ "zen" "emacs" "kitty" ];
        };
        wallpaper_dir = "~/Pictures/Wallpapers";
    };

    my-home.dev.vscodium = {
        enable = true;
    };

    my-home.de.themes = {
        enable = true;
        opacity = 0.8;
    };
}
