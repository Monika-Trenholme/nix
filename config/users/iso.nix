{ outputs, ... }:
{
    imports = [ outputs.homeModules.default ];

    my-home.dev= {
        emacs.enable = true;
        git.enable = true;
    };

    my-home.shell = {
        prompt = [
            {
                name = "username";
                style_user = "green";
                style_root = "red";
                format = "[$user]($style)";
                disabled = false;
                show_always = true;
            }{
                name = "hostname";
                format = " @ [$hostname](yellow)";
                disabled = false;
                ssh_only = false;
            }{
                name = "directory";
                format = " : [$path](blue)";
                home_symbol = "~";
                disabled = false;
            }
        ];
        fetch.enable = true;
    };

    my-home.de = {
        enable = true;
        tags = [ "" "󰅩" "" "4" "5" "6" "7" "8" "9" "0" ];
        keyboard = "us";
        wm = {
            gaps_in = 10;
            gaps_out = 20;
            auto_start = [ "zen" "emacs" "kitty" ];
        };
        wallpaper_dir = "~/Pictures/Wallpapers";
    };

    my-home.de.themes = {
        enable = true;
        opacity = 0.8;
    };
}
