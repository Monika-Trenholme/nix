{ config, lib, pkgs, ... }:
{
	imports = [ ./hardware-configuration.nix ];

    # Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    
	# Unfree
	nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    	# Drivers
    	"cnijfilter2"
    	# Games
    	"osu-lazer-bin" "vintagestory"
        # Steam
        "steam" "steam-original" "steam-unwrapped" "steam-run"
	];

	# Keyboard
	console.keyMap = "us";

	# User
	my-os.users.admins = [ "lpt4" "monika" ];

	# My services
	networking.hostName = "framework16";
	my-os.services = {
	    networking = {
			enable = true;
	        network-manager.enable = true;
	    };
	    audio.enable = true;
		printing = {
			enable = true;
			drivers = with pkgs; [ cnijfilter2 ];
		};
		uinput.enable = true; # Required for Xremap
		xdg-portal.enable = true;
	};

	# My programs
	my-os.programs = {
		games.enable = true;
		programming.enable = true;
		virtualisation.enable = true;
		web.enable = true;
	};

	# Other packages
	environment.systemPackages = with pkgs; [
		# Misc
		mate.atril ffmpeg handbrake filezilla remmina imagemagick gimp
		(pkgs.wrapOBS {
			plugins = with pkgs.obs-studio-plugins; [
      			wlrobs
      			obs-backgroundremoval
      			obs-pipewire-audio-capture
    		];
		})

		# Android
		android-tools

		# NixOS Generators for generating isos
		nixos-generators
	];

	# Nix
	system.stateVersion = "25.05";
	nix.settings.experimental-features = ["nix-command" "flakes"];
	nix.gc = {
		automatic = true;
		dates = "weekly";
		options = "--delete-older-than 30d";
	};
}
