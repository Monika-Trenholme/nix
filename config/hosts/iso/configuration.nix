{ lib, outputs, pkgs, inputs, ... }:
with lib;
{
    # Virtualisation
    services.qemuGuest.enable = true;
    services.spice-vdagentd.enable = true;  # enable copy and paste between host and guest

    # Keyboard
    console.keyMap = "uk";

    # User
    my-os.users = {
        admins = [ "iso" ];
        autologin = "iso";
    };

    # Services
    networking.hostName = "iso";
    my-os.services = {
        audio.enable = true;
        networking = {
            enable = true;
            network-manager.enable = true;
            ssh.enable = true;
        };
        uinput.enable = true; # Required for Xremap
    };
    networking.wireless.enable = false; # Disable wpa_supplicant which conflicts with network manager

    # Packages
    environment.systemPackages = with pkgs; [
        # Browser
        inputs.zen-browser.packages."${system}".default

        # Initialisation script
        (writeShellScriptBin "init" ''
            # Grab the disk module for the given system configuration
            curl "https://gitlab.com/Monika-Trenholme/nix/-/raw/main/disks/$1.nix?ref_type=heads" > /tmp/disk.nix

            # Format the disk
            sudo ${disko}/bin/disko --mode destroy,format,mount /tmp/disk.nix --arg device "\"$2\""

            # Generate system configuration
            sudo nixos-generate-config --no-filesystems --root /mnt
            sudo mkdir /tmp/install -p
            sudo mv /mnt/etc/nixos/*.nix /tmp/install/

            # Clone git repo
            sudo ${git}/bin/git clone https://gitlab.com/Monika-Trenholme/nix /mnt/etc/nixos
            sudo mv /tmp/install/ /mnt/etc/nixos/
        '')

        # Installation script
        (writeShellScriptBin "strap" ''
            sudo nixos-install --root /mnt --flake "/mnt/etc/nixos#$1"
        '')
    ];

    # Enable flakes
    nix.settings.experimental-features = ["nix-command" "flakes"];
}
