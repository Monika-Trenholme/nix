{ osConfig, lib, pkgs, ... }:
with lib;
{
    imports = [
        ./de
        ./dev
        ./shell.nix
    ];

    config = mkMerge [
        {
            # Configure Virt-Manager to use QEMU+KVM
            dconf.settings = {
                "org/virt-manager/virt-manager/connections" = {
                    autoconnect = ["qemu:///system"];
                    uris = ["qemu:///system"];
                };
            };
            home.packages = with pkgs; [ dconf ];
        }
        (mkIf (osConfig ? system.stateVersion) { home.stateVersion = osConfig.system.stateVersion; })
    ];
}
