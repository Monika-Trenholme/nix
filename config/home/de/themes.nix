{ config, lib, pkgs, ... }:
with lib;
let
    decfg = config.my-home.de;
    cfg = decfg.themes;
in
{
    options.my-home.de.themes = with types; {
        enable = mkEnableOption "Themes";
        nerd-font = {
            package = mkOption {
                type = package;
                description = "Nerd Font to use";
                default = pkgs.nerd-fonts.lilex;
            };
            name = mkOption {
                type = str;
                description = "Name of Nerd Font to use";
                default = "Lilex Nerd Font";
            };
        };
        opacity = mkOption {
            type = float;
            description = "Background opacity of supported apps";
            default = 1.0;
        };
    };

    config = mkIf cfg.enable {
        home.packages = with pkgs; [
            cfg.nerd-font.package
        ];
        fonts.fontconfig.enable = true;
        dconf.settings = {
            "org/gnome/desktop/background" = {
                picture-uri-dark = "file://${pkgs.nixos-artwork.wallpapers.nineish-dark-gray.src}";
            };
            "org/gnome/desktop/interface" = {
                color-scheme = "prefer-dark";
            };
        };
        gtk = {
            enable = true;
            theme = {
                name = "Adwaita-dark";
                package = pkgs.gnome-themes-extra;
            };
        };
    };
}
