{ config, pkgs, lib, ... }:
with lib;
let
    cfg = config.my-home.de;
    themecfg = cfg.themes;
in
{
    imports = [
        ./bar.nix
        ./keybinds.nix
        ./menus.nix
        ./mimeapps.nix
        ./themes.nix
        ./wm.nix
    ];

    options.my-home.de = with types; {
        enable = mkEnableOption "desktop environment";

        tags = mkOption {
            type = listOf str;
            description = "Tag names";
            default = [ "1" "2" "3" "4" "5" "6" "7" "8" "9" "0" ];
        };
        
        keyboard = mkOption {
            type = str;
            description = "Keyboard layout";
            default = "us";
        };

        wallpaper_dir = mkOption {
            type = uniq str;
            description = "location of wallpaper directory";
            default = "~/Pictures/Wallpapers";
        };
    };

    config = mkIf cfg.enable {
        # Packages depended on by the DE configuration
        home.packages = with pkgs; [
            cliphist grim wl-clipboard wofi way-displays
        ];

        # Notifications
        services.mako = {
            enable = true;
            font = "${themecfg.nerd-font.name} 12";
        };

        # Screenshots
        services.flameshot = {
            enable = true;
            package = pkgs.flameshot.overrideAttrs (oldAttrs: {
                cmakeFlags = [
                    "-DUSE_WAYLAND_CLIPBOARD=1"
                    "-DUSE_WAYLAND_GRIM=1"
                ];
                buildInputs = oldAttrs.buildInputs ++ [ pkgs.libsForQt5.kguiaddons ];
            });
            settings = {
                General = {
                    disabledGrimWarning = true;
                    disabledTrayIcon = true;
                    showStartupLaunchMessage = false;
                };
            };
        };

        # Terminal
        programs.kitty = {
            enable = true;
            settings = {
                background_opacity = themecfg.opacity;
                font_family = themecfg.nerd-font.name;
            };
        };
        programs.zsh.shellAliases = {
            s = "kitten ssh";
        };

        # Lock screen & wall paper
        programs.hyprlock = {
            enable = true;
            settings ={
                general = {
                    disable_loading_bar = true;
                    grace = 15;
                    hide_cursor = true;
                    no_fade_in = false;
                };

                background = [
                    {
                        path = "screenshot";
                        blur_passes = 0;
                        blur_size = 8;
                    }
                ];

                input-field = [
                    {
                        size = "200, 50";
                        position = "0, -80";
                        monitor = "";
                        dots_center = true;
                        fade_on_empty = false;
                        font_color = "rgb(202, 211, 245)";
                        inner_color = "rgb(91, 96, 120)";
                        outer_color = "rgb(24, 25, 38)";
                        outline_thickness = 5;
                        placeholder_text = "Password...";
                        shadow_passes = 2;
                    }
                ];
            };
        };
        services.wpaperd = {
            enable = true;
            settings = {
                eDP-1.path = cfg.wallpaper_dir;
            };
        };

        # Session Variables
        home.sessionVariables = {
            # General
            HYPRCURSOR_SIZE=25;

            # Session variables for compatibility
            ECORE_EVAS_ENGINE = "wayland_egl";
            ELM_ENGINE = "wayland_egl";
            SDL_VIDEODRIVER = "wayland";
            MOZ_ENABLE_WAYLAND = "1";
            QT_QPA_PLATFORM = "wayland";
            _JAVA_AWT_WM_NONREPARENTING = "1";
        };
    };
}
