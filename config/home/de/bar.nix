{ config, lib, pkgs, inputs, ... }:
with lib;
let
    decfg = config.my-home.de;
    cfg = decfg.bar;
    themecfg = decfg.themes;

    bar = with pkgs; writeShellApplication {
        name = "bar";
        runtimeInputs = [ sandbar ];
        text = builtins.readFile ./bar.sh;
    };
in
{
    options.my-home.de.bar = with types; {
        laptop = {
            enable = mkEnableOption "backlight module";
            backlight = mkOption {
                type = str;
                description = "name of backlight device";
                default = "";
            };
            battery = mkOption {
                type = str;
                description = "name of battery device";
                default = "BAT0";
            };
        };
    };

    config = mkIf decfg.enable {
        my-home.de.wm.auto_start = [
            ''
                '${inputs.status.packages.${pkgs.system}.default}/bin/status | \
                ${pkgs.sandbar}/bin/sandbar \
	                -font "${themecfg.nerd-font.name}:pixelsize=16" \
                    -tags ${toString (length decfg.tags)} ${concatStringsSep " " decfg.tags} \
                &'
            ''
        ];
    };
}
