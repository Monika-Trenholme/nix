{ config, lib, pkgs, ... }:
with lib;
let
    decfg = config.my-home.de;
    cfg = config.my-home.de.wm;
in
{
    options.my-home.de.wm = with types; {
        gaps_in = mkOption {
            type = int;
            description = "Size of gap between windows";
            default = 10;
        };
        gaps_out = mkOption {
            type = int;
            description = "Size of gap between windows and screen edge";
            default = 20;
        };
        auto_start = mkOption {
            type = listOf str;
            description = "List of programs to start automatically";
            default = [];
        };
	};

	config = mkIf decfg.enable {
        wayland.windowManager.river = {
            enable = true;
            xwayland.enable = false;
        };

        # Layout
        home.packages = with pkgs; [ river-luatile ];
        home.file.river-luatile = {
            target = ".config/river-luatile/layout.lua";
            text = builtins.readFile ./layout.lua;
        };

        # riverctl commands
        wayland.windowManager.river.settings = {
            # Layout
            default-layout = "luatile";

            # Input
            keyboard-layout = decfg.keyboard;
            input."pointer-1739-0-Synaptics_TM3053-003".events = "disabled";

            # Misc
            focus-follows-cursor = "always";
            map-switch.normal.lid.close = "spawn hyprlock";

            # App rules
            rule-add = {
                "-app-id zen" = "tags 1";
                "-app-id emacs -title '*(*'" = "tags $((1 << 1))";
                "-app-id kitty" = "tags $((1 << 2))";
                "-app-id *stremio*" = "tags $((1 << 4))";

                # Games
                "-app-id 'osu!'" = [ "tags $((1 << 3))" "no-fullscreen" ];
            };

            # Autostart
            spawn = [
                # Daemons
                "'river-luatile &'" # Layout generator
                "'wpaperd &'"       # Wallpaper
                "'way-displays > /tmp/way-displays.\${XDG_VTNR}.\${USER}.log 2>&1 &'" # Displays
            ] ++ cfg.auto_start;
        };
        wayland.windowManager.river.extraConfig = ''
            riverctl send-layout-cmd luatile 'set_gaps(${toString cfg.gaps_out}, ${toString cfg.gaps_in})'
        '';
	};
}
