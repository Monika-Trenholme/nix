{ config, lib, pkgs, ... }:
let
    decfg = config.my-home.de;
    cfg = decfg.mimeapps;
    
    emacs = config.programs.emacs.finalPackage;
    emacs-dired = pkgs.writeShellScriptBin "emacs-dired.sh"
        ''${emacs}/bin/emacs --eval "(dired \"$1\")"'';
in
with lib;
{
    options.my-home.de.mimeapps = with types; {
        enable = mkOption {
            type = bool;
            default = decfg.enable;
            description = "Whether to enable mimeapps";
        };
    };

    config = mkIf cfg.enable {
        xdg.desktopEntries = {
            emacs-dired = {
                name = "Dired";
                genericName = "File Manager";
                exec = ''${emacs-dired}/bin/emacs-dired.sh'';
            };
        };
        xdg.mimeApps.enable = true;
        xdg.mimeApps.defaultApplications = {
            "application/x-directory" = "emacs-dired.desktop";
            "inode/directory" = "emacs-dired.desktop";
        };
    };
}
