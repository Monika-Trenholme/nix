local gaps_out = 0
local gaps_in = 0
local main_ratio = 0.65

-- The most important function - the actual layout generator
--
-- The argument is a table with:
--  * Focused tags (`args.tags`)
--  * Window count (`args.count`)
--  * Output width (`args.width`)
--  * Output height (`args.height`)
--  * Output name (`args.output`)
--
-- The return value must be a table with exactly `count` entries. Each entry is a table with four
-- numbers:
--  * X coordinate
--  * Y coordinate
--  * Window width
--  * Window height
--
-- This example is a simplified version of `rivertile`
function handle_layout(args)
	local retval = {}
	if args.count == 1 then
		table.insert(retval, { gaps_out, 0, args.width - gaps_out * 2, args.height - gaps_out })
	elseif args.count > 1 then
		local main_w = (args.width - gaps_out * 2 - gaps_in) * main_ratio
		local side_w = (args.width - gaps_out * 2 - gaps_in) - main_w
		local main_h = args.height - gaps_out
		local side_h = (args.height - (gaps_out - gaps_in)) / (args.count - 1) - gaps_in
		table.insert(retval, {
			gaps_out,
			0,
			main_w,
			main_h,
		})
		for i = 0, (args.count - 2) do
			table.insert(retval, {
				main_w + gaps_out + gaps_in,
				i * (side_h + gaps_in),
				side_w,
				side_h,
			})
		end
	end
	return retval
end

-- Return the name of the current layout
function handle_metadata(args)
    return { name = "luatile" }
end

-- Set pixel size of gaps
function set_gaps(out, _in)
    gaps_out = out
    gaps_in = _in
end

-- Increase/decrease ratio of the main view and the side views
function ratio_delta(delta)
    main_ratio = main_ratio + delta
    if main_ratio < delta then
        main_ratio = delta
    elseif main_ratio > 1.0 then
        main_ratio = 1.0
    end
end
