{ config, inputs, lib, ... }:
with lib;
let
    decfg = config.my-home.de;
in
{
    imports = [ inputs.xremap-flake.homeManagerModules.default ];

    config = {
        services.xremap = {
            enable = decfg.enable;
            withWlroots = true;
            config = {
                modmap = [
                    {
                        name = "default";
                        remap = { CapsLock = "Control_L"; };
                    }
                ];
                keymap = [
                    {
                        name = "emacs-like";
                        application = { not = "emacs"; };
                        remap = {
                            # Emacs like keybinds
                            C-b = "left";
                            C-f = "right";
                            C-p = "up";
                            C-n = "down";
                        };
                    }
                ];
            };
        };

        wayland.windowManager.river.settings.declare-mode = [ "passthrough" ];
        wayland.windowManager.river.settings.map = {
            passthrough."Super Escape" = "enter-mode normal";
            normal = mkMerge (
                # Tag control
                (builtins.map(n:
                    let
                        key = toString n;
                        tag = "$((1 << ${toString (if n == 0 then 9 else n - 1)}))";
                    in {
                        "Super ${key}" = "set-focused-tags ${tag}";
                        "Super+Shift ${key}" = "set-view-tags ${tag}";
                        "Super+Control ${key}" = "toggle-focused-tags ${tag}";
                    }
                ) [ 1 2 3 4 5 6 7 8 9 0 ])
                ++
                [{
                    # Audio
                    "None XF86AudioRaiseVolume" = "spawn 'wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+'";
                    "None XF86AudioLowerVolume" = "spawn 'wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-'";
                    "None XF86AudioMute"        = "spawn 'wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle'";

                    # Print screen
                    "None Print"  = "spawn 'flameshot gui --raw | wl-copy'";

                    # Launch Menus
                    "Super R" = "spawn 'wofi --show drun'"; # Launch application menu
                    "Super P" = "spawn keepmenu";           # Launch passwords menu

                    # Launch Programs
                    "Super Return" = "spawn kitty";
                    "Super F" = "spawn 'xdg-open ~'";
                    "Super L" = "spawn hyprlock";

                    # WM control
                    "Super Escape"  = "enter-mode passthrough";
                    "Super+Shift E" = "exit";                     # Exit window manager
                    "Super+Shift Q" = "close";                    # Close focused window
                    "Super+Shift F" = "toggle-fullscreen";        # Toggle full screen
                    "Super+Shift N" = "swap next";                # Swap with next view in stack
                    "Super+Shift P" = "swap previous";            # Swap with previos view in stack
                    "Super Equal"   = "send-layout-cmd luatile 'ratio_delta(0.05)'";
                    "Super Minus"   = "send-layout-cmd luatile 'ratio_delta(-0.05)'";

                    # Input & Output
                    "Super+Control S" = "keyboard-layout ${decfg.keyboard}";
                    "Super+Control I" = "keyboard-layout -variant intl ${decfg.keyboard}";
                }]
            );
        };

        wayland.windowManager.river.settings.map-pointer.normal = {
            "Super BTN_LEFT"   = "move-view";
            "Super BTN_MIDDLE" = "toggle-float";
        };
    };
}
