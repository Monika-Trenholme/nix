{ config, lib, pkgs, ... }:
with lib;
let
    cfg = config.my-home.de.menu;
in
{
    options.my-home.de.menu = {
        passwords.enable = mkEnableOption "keepass passwords menu";
    };

    config = mkIf cfg.passwords.enable {
        home.packages = with pkgs; [ keepmenu wtype ];

        # Config for keepmenu
        home.file.keepmenu = {
            target = ".config/keepmenu/config.ini";
            text = ''
                [dmenu]
                dmenu_command = wofi -w=30 -d -i

                [dmenu_passphrase]
                obscure = True

                [database]
                database_1 = ~/Passwords.kdbx
                autotype_default = {USERNAME}{TAB}{PASSWORD}{ENTER}
                type_library = wtype
            '';
        };
    };
}
