{ config, lib, ... }:
with lib;
let
    cfg = config.my-home.shell;
    decfg = config.my-home.de;
in
{
    options.my-home.shell = with types; {
        fetch.enable = mkEnableOption "fetch";
        prompt = mkOption {
            type = listOf (attrsOf anything);
            description = "Prompt config";
            default = [
                {
                    name = "directory";
                    format = "[$path](blue)";
                    home_symbol = "~";
                    disabled = false;
                }
            ];
        };
    };

    config = {
        programs.zsh = {
            enable = true;
            enableCompletion = true;
            syntaxHighlighting.enable = true;
            autosuggestion.enable = true;
            shellAliases = {
                rebuild = "sudo nixos-rebuild";
            };
            initExtra = mkMerge [
                (mkIf cfg.fetch.enable ''fastfetch'')
                (mkIf decfg.enable ''
                    if [ -z "$WAYLAND_DISPLAY" ] && [ $(tty) = "/dev/tty1" ]; then
                        exec river
                    fi
                '')
            ];
        };

        programs.starship.enable = true;
        programs.starship.settings = mkMerge [
            {
                format = concatStrings (map (set: "$" + set.name) (cfg.prompt) ++ ["$line_break" "$character"]);
                character = {
                    success_symbol = "->";
                    error_symbol = "->";
                };
            }
            (listToAttrs (map (set: { name = set.name; value = removeAttrs set ["name"]; }) cfg.prompt))
        ];

        programs.fastfetch.enable = cfg.fetch.enable;
        programs.fastfetch.settings = {
            modules = [
                {
                    type = "title";
                    color = {
                        user = "32";
                        host = "33";
                    };
                }
                "separator"

                "locale"
                "shell"
                "terminalfont"
                "terminal"
                "wm"
                "break"

                "packages"
                "os"
                "kernel"
                "bios"
                "break"

                "uptime"
                "localip"
                "battery"
                "disk"
                "break"

                "display"
                "memory"
                "gpu"
                "cpu"
                "board"
                "break"

                "break"
                "colors"
            ];
        };
    };
}
