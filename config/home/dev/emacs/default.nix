{ config, lib, pkgs, ... }:
with lib;
let
    cfg = config.my-home.dev.emacs;
    langcfg = cfg.languages;
    themecfg = config.my-home.de.themes;
    mkLanguageOption = name: with types; mkOption {
        type = bool;
        description = "Whether to enable ${name} support";
        default = langcfg.all;
    };
in
{
    options.my-home.dev.emacs = with types; {
        enable = mkEnableOption "emacs";
        languages = {
            all = mkEnableOption "all language support";
            rust = mkLanguageOption "rust";
            web = mkLanguageOption "web";
        };
    };
    config.programs.emacs = {
        enable = cfg.enable;
        package = pkgs.emacs-pgtk;
        extraPackages = epkgs: with epkgs; [
            # project management
            projectile magit

            # auto-complete
            ivy

            # Theme
            monokai-theme ligature

            # Extra
            elfeed

            # Packages needed for config
            nix-mode lua-mode
        ]
        ++ optionals langcfg.rust [ rust-mode pkgs.rust-analyzer pkgs.rustfmt ]
        ++ optionals langcfg.web [ web-mode pkgs.typescript-language-server ];
        extraConfig = mkIf themecfg.enable ''
            (set-face-attribute 'default nil :font "${themecfg.nerd-font.name}" :height 130)
            (set-frame-parameter (selected-frame) 'alpha-background ${toString themecfg.opacity})
            ${builtins.readFile ./emacs.el}
        '';
    };
}
