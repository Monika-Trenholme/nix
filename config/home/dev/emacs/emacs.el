;; View

;;;; title
(setq frame-title-format "%f (%m)")
(add-hook 'dired-mode-hook (lambda () (setq-local frame-title-format "%b")))

;;;; remove window decorations
(setq default-frame-alist '((undecorated . t)))

;;;; dark mode
(when (display-graphic-p)
  (invert-face 'default))
(set-variable 'frame-background-mode 'dark)

;;;; Disable annoying modes
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;;;; Enable useful modes
(global-display-line-numbers-mode 1)
(column-number-mode 1)
(show-paren-mode 1)

;;;; Tab width
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)
(add-hook 'nix-mode-hook (lambda () (setq tab-width 4)))

;;;; Theme
(load-theme 'monokai t)
(use-package ligature
  :enure t
  :config
  (ligature-set-ligatures '(prog-mode
                            org-mode
                            lsp
                            cc-mode
                            haskell-mode
                            emacs-lisp-mode)
                          '("->" "<-" "-->" "<--" "<-->"
                            "|>" "<|" "=>" "==>" "::"
                            "==" "!=" "<=" ">=" "===" "!=="))
  (global-ligature-mode t))

;; Misc

;;;; Remove splash
(setq-default inhibit-splash-screen t)

;;;; Set auto save directory
(setq backup-directory-alist `(("." . "~/.saves")))

;;;; Use y/n instead of yes/no
(setq use-short-answers t)

;;;; View urls in eww by default
(setq browse-url-browser-function 'eww-browse-url)

;; Project management

;;;; Projectile
(use-package projectile
  :ensure t
  :bind-keymap (("C-c p" . projectile-command-map))
  :custom ((projectile-completion-system 'ivy))
  :init
  (when (file-directory-p "~/Code")
    (setq projectile-project-search-path '("~/Code/"))))
(projectile-mode +1)

;; Modes

;;;; Dired
(global-set-key (kbd "C-x j") 'dired-jump)
(use-package dired
  :ensure nil
  :hook
  (dired-mode . auto-revert-mode) ; Auto-refresh dired on file listing change
  :config
  (define-key dired-mode-map (kbd "C-f") 'dired-find-file)
  (define-key dired-mode-map (kbd "C-b") 'dired-up-directory))

;;;; Rust
(use-package rust-mode
  :hook
  (rust-mode . eglot-ensure))

;;;; Web
(use-package web-mode
  :hook
  (js-mode . eglot-ensure)
  :mode
  ("\\.html?\\'" . web-mode)
  ("\\.[c|m]js[x]?\\'" . js-mode))

;; Apps

;;;; Elfeed
(global-set-key (kbd "C-x w") 'elfeed)
(setq elfeed-feeds
      '(("https://vimuser.org/feed.xml" blog rowe)))
