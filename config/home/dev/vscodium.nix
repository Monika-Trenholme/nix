{ config, pkgs, lib, ... }:
with lib;
let
    cfg = config.my-home.dev.vscodium;
in
{
    options.my-home.dev.vscodium = {
        enable = mkEnableOption "vscodium";
    };
    config.programs.vscode = {
        enable = cfg.enable;
        package = pkgs.vscodium;
        profiles.default = {
            extensions = with pkgs.vscode-extensions; [
                bbenoist.nix
                rust-lang.rust-analyzer
            ];
            userSettings = {
                "editor.fontLigatures" = true;
                "editor.inlayHints.enabled" = false;
            };
        };
    };
}
