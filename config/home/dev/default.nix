{ ... }:
{
    imports = [
        ./emacs
        ./git.nix
        ./vscodium.nix
    ];
}
