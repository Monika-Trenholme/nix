{ config, lib, ... }:
with lib;
let
    cfg = config.my-home.dev.git;
in
{
    options.my-home.dev.git = with types; {
        enable = mkEnableOption "git";
        user_name = mkOption {
            type = nullOr str;
            description = "Git username";
            default = null;
        };
        user_email = mkOption {
            type = nullOr str;
            description = "Git user email";
            default = null;
        };
    };

    config.programs.git = {
        enable = cfg.enable;
        userName = cfg.user_name;
        userEmail = cfg.user_email;
        extraConfig = {
            init.defaultBranch = "main";
        };
    };
}
