{
  device ? throw "Set this to your disk device, e.g. /dev/sda",
  ...
}: {
    disko.devices = {
        disk.main = {
            inherit device;
            type = "disk";
            content = {
                type = "gpt";
                partitions = {
                    root = {
                        name = "luks";
                        size = "100%";
                        content = {
                            type = "luks";
                            name = "crypted";
                            extraOpenArgs = [ "--allow-discards" ];
                            keyFile = "/tmp/secret.key";
                            content = {
                                type = "lvm_pv";
                                vg = "grubcrypt";
                            };
                        };
                    };
                };
            };
        };
        lvm_vg.grubcrypt = {
            type = "lvm_vg";
            lvs = {
                bootvol = {
                    size = "500M";
                    content = {
                        type = "filesystem";
                        format = "vfat";
                        mountpoint = "/boot";
                    };
                };
                rootvol = {
                    size = "100%FREE";
                    content = {
                        type = "btrfs";
                        extraArgs = ["-f"];
                        subvolumes = {
                            "/root" = {
                                mountpoint = "/";
                            };

                            "/persist" = {
                                mountOptions = ["subvol=persist" "noatime"];
                                mountpoint = "/persist";
                            };

                            "/nix" = {
                                mountOptions = ["subvol=nix" "noatime"];
                                mountpoint = "/nix";
                            };
                        };
                    };
                };
            };
        };
    };
}
