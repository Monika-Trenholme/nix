{
    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
        home-manager = {
            url = github:nix-community/home-manager;
            inputs.nixpkgs.follows = "nixpkgs";
        };
        nixos-hardware.url = github:NixOS/nixos-hardware/master;
        status.url = gitlab:Monika-Trenholme/status;
        disko = {
			url = "github:nix-community/disko";
			inputs.nixpkgs.follows = "nixpkgs";
        };
        xremap-flake.url = github:xremap/nix-flake;
        zen-browser.url = "github:VCPYC/zen-browser-flake";
    };

    outputs = {self, nixpkgs, nixos-hardware, ...}@inputs:
    let
        outputs = self.outputs;
        mkSystem = system: modules: nixpkgs.lib.nixosSystem {
            inherit system;
            specialArgs = { inherit inputs outputs; };
            modules = [
                outputs.systemModules.default
                inputs.home-manager.nixosModules.home-manager
                inputs.disko.nixosModules.default
            ] ++ modules;
        };
        mkUser = username: config: {
            home-manager.extraSpecialArgs = { inherit inputs outputs; };
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.${username} = import config;
            users.users.${username} = {
		        isNormalUser = true;
	        };
        };
    in
    {
        nixosConfigurations = {
            t440p = mkSystem "x86_64-linux" [
                ./config/hosts/t440p/configuration.nix
                nixos-hardware.nixosModules.lenovo-thinkpad-t440p
                (import ./config/disks/t440p.nix { device = "/dev/sda"; })
                (mkUser "monika" ./config/users/monika/laptop.nix)
            ];

            framework16 = mkSystem "x86_64-linux" [
                ./config/hosts/framework16/configuration.nix
                (mkUser "lpt4" ./config/users/lpt4/desktop.nix)
                (mkUser "monika" ./config/users/monika/laptop.nix)
            ];

            iso = mkSystem "x86_64-linux" [
                ./config/hosts/iso/configuration.nix
                (nixpkgs + "/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix")
                (mkUser "iso" ./config/users/iso.nix)
            ];
        };

        homeModules.default = ./config/home;
        systemModules.default = ./config/system;
    };
}
